<?php
# src/Entity/Address.php

namespace tpdoctrine\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(
    name="address"
 )
*/

 class Address
 {
    /**
    * @ORM\id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $id;

    /**
    * @ORM\Column(type="integer")
    */
    protected $appart;

    /**
    * @ORM\Column(type="string")
    */
    protected $street;

    /**
    * @ORM\Column(type="string")
    */
    protected $zipcode;

    /**
    * @ORM\Column(type="string")
    */
    protected $city;

    /**
    * @ORM\Column(type="string")
    */
    protected $country;

    /**
    * @ORM\OneToOne(targetEntity=User::class, mappedBy="address")
    */
    protected $user;


    // getters et setters

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $idA
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAppart()
    {
        return $this->appart;
    }

    /**
     * @param mixed $appart
     *
     * @return self
     */
    public function setAppart($appart)
    {
        $this->appart = $appart;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     *
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * @param mixed $zipcode
     *
     * @return self
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     *
     * @return self
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    public function __toString()
    {
        $format = "User (id: %s, appart: %s, street: %s, zipcode: %s, city: %s, country: %s, user: %s)\n";
        return sprintf($format, $this->id, $this->appart, $this->street, $this->zipcode, $this->city, $this->country, $this->user);
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
