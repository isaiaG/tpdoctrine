<?php
# src/Entity/Poll.php

namespace tpdoctrine\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
* @ORM\Entity
* @ORM\Table(name="polls")
*/

class Poll
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue
    * @ORM\Column(type="integer")
    */
    protected $id;

    /**
    * @ORM\Column(type="text")
    */
    protected $title;

    /**
    * @ORM\Column(type="datetime")
    */
    protected $created;

    /**
    * @ORM\OneToMany(targetEntity=Question::class, cascade={"persist", "remove"}, mappedBy="poll"))
    */
    protected $questions;

    // le constructeur crée la collection de réponses
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    public function __toString()
    {
        $format = "Question (id: %s, title: %s, created: %s, question: %s)\n";

        $ques ="";
        foreach ($this->questions as $q) {
            $ques = $ques . $q;
        }

        return sprintf($format, $this->id, $this->title, $this->created->format('Y-m-d H:i:s'), $ques);
    }

    // getters et setters



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    public function getQuestions()
    {
        return $this->answers;
    }
     
    public function addQuestion(Question $question)
    {
        $this->questions->add($question);
        $question->setPoll($this);
    }
}

