<?php
# create-address.php

$entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

use tpdoctrine\Entity\User;
use tpdoctrine\Entity\Address;

// Instanciation de l'adresse

$userRepo = $entityManager->getRepository(User::class);

$user = $userRepo->find(3);

$address = new Address();
$address->setAppart("1");
$address->setStreet("Rue de la rue");
$address->setZipcode("45000");
$address->setCity("Orléans");
$address->setCountry("France");

$user->setAddress($address);

// Gestion de la persistance
$entityManager->persist($address);
$entityManager->flush();

// Vérification du résultats
echo "Identifiant de l'utilisateur créé : ", $address->getId();
