<?php
# get-users.php

$entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

use tpdoctrine_guille\Entity\User;

$userRepo = $entityManager->getRepository(User::class);

$user = $userRepo->find(1);
echo "User by primary key:<BR/>";
echo $user;

$allUsers = $userRepo->findAll();
echo "All users:<BR/>";
foreach ($allUsers as $user) {
    echo $user.'<BR/>';
}

$usersByRole = $userRepo->findBy(["role" => "admin"]);
echo "Users by role:<BR/>";
foreach ($usersByRole as $user) {
    echo $user.'<BR/>';
}

$usersByRoleAndFirstname = $userRepo->findBy(["role" => "user", "firstname" => "First 2"]);
echo "Users by role and firstname:<BR/>";
foreach ($usersByRoleAndFirstname as $user) {
    echo $user.'<BR/>';
}
