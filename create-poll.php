<?php
# create-poll.php

$entityManager = require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'bootstrap.php']);

use tpdoctrine\Entity\Poll;
use tpdoctrine\Entity\Question;
use tpdoctrine\Entity\Answer;

// Instanciation de la question

$date = new DateTime('2012-01-01', new DateTimeZone('Europe/Paris'));

$po = new Poll();
$po->setTitle("Le super sondage");
$po->setCreated($date);

$qu = new Question();
$qu->setWording("Doctrine 2 est-il un bon ORM ?");

$ans1=new Answer();
$ans1->setWording("Oui, bien sûr !");

$ans2=new Answer();
$ans2->setWording("Non, peut mieux faire !");

$po->addQuestion($qu);
$qu->addAnswer($ans1);
$qu->addAnswer($ans2);
// Gestion de la persistance
$entityManager->persist($po);
$entityManager->flush();
// Vérification du résultat
echo $po;
